# ADSpinnaker conda recipe

Home: "https://github.com/areaDetector/ADSpinnaker"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS areaDetector driver for cameras from FLIR (formerly Point Grey) using their Spinnaker SDK

#!/bin/bash

# LIBVERSION shall only include MAJOR.MINOR.PATCH for require
LIBVERSION=$(echo ${PKG_VERSION}| cut -d'.' -f1-3)

# Clean between variants builds
make clean

# To use the libraries that come in https://github.com/areaDetector/ADSpinnaker/tree/master/spinnakerSupport
# set SUPPORT_EXTERNAL to NO and install the VENDOR LIBS
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} SUPPORT_EXTERNAL=NO vlibs
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} SUPPORT_EXTERNAL=NO
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} SUPPORT_EXTERNAL=NO db
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} SUPPORT_EXTERNAL=NO install
# With SUPPORT_EXTERNAL=YES, we use the libs that come with the spinnaker-sdk conda package
#make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} SUPPORT_EXTERNAL=YES
#make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} SUPPORT_EXTERNAL=YES db
#make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} SUPPORT_EXTERNAL=YES install

